**Алгоритм выхода из лабиринта**

Построен лабиринт и реализован алгоритм выхода из лабиринта для мобильного робота turtlebot3



Для запуска необходимо проделать следующие шаги:

1. mkdir catkin_ws
2. cd catkin_ws
3. git clone repository 
4. source  devel/setup.bash
5. export TURTLEBOT3_MODEL=waffle
6. roslaunch turtlebot3_gazebo maze_3_world.launch
7. create new terminal
8. source  devel/setup.bash
9. python3 maze_explorer.py



<img src="maze.jpg" width="480"/>
