#!/usr/bin/env python

from math import inf
import rospy, time
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan

import time

left_side_sens=0
right_side_sens=0
front_side_sens=0


out_maze=0

def scan_callback(msg):
   
   


    global left_side_sens
    global right_side_sens
    global front_side_sens
    global diag_left_side_sens
    global diag_right_side_sens
    
    ranges = msg.ranges

    

    right_side_sens=min(ranges[265:275])
    print("left",left_side_sens)
    left_side_sens = min(ranges[85:95])
    print("front",front_side_sens)
    front_side_sens = min(ranges[345:360]+ranges[0:15])
    print("right",right_side_sens)



# Create the node
cmd_vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size = 1) # to move the robot
scan_sub = rospy.Subscriber('scan', LaserScan, scan_callback)   # to read the laser scanner
rospy.init_node('maze_explorer')

command = Twist()




while not rospy.is_shutdown():

    if (out_maze==1):
        print ("You are win")
    
    else:

 
        if (front_side_sens >= 1.2):
            # Front is free
            command.angular.z = 0
            command.linear.x = 0.8
            cmd_vel_pub.publish(command)

            if (right_side_sens <= 0.2):
                # turn left if it's possible
                command.angular.z = 0.5
                command.linear.x = 0.4
                cmd_vel_pub.publish(command)

            if (left_side_sens <= 0.2):
                # turn right if it's possible
                command.angular.z = -0.5
                command.linear.x = 0.4
                cmd_vel_pub.publish(command)

            if (right_side_sens >= 0.3):
                # turn right if it's possible
                command.angular.z = -0.5
                command.linear.x = 0.4
                cmd_vel_pub.publish(command)

        elif (front_side_sens < 1.2):
            # turn left
            command.angular.z = 1
            command.linear.x = 0
            cmd_vel_pub.publish(command)
        
        

      

       



